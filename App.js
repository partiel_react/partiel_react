import { StatusBar } from 'expo-status-bar';
import React, {useState, useRef } from 'react';
import { StyleSheet, Text, View, TextInput, Button, ScrollView, FlatList, TouchableOpacity } from 'react-native';

export default function App() {

  const [jeux, setJeux] = useState([
    {
      name: "Medal of Honor",
      price: "10€",
      catégorie : 'FPS',
      id: 23124
    },
    {
      name: "Street Fighter 2",
      price: "20€",
      catégorie : "Combat",
      id: 12349
    },
    {
      name: "Call of Duty",
      price: "30€",
      catégorie : "FPS",
      id: 549762
    },

    {
      name: "NBA2K5",
      price: "5€",
      catégorie : "Sport",
      id: 549763
    },

    {
      name: "God Of War 2018",
      price: "25€",
      catégorie : "Action-Aventure",
      id: 549764
    },

    {
      name: "The Legend of Zelda : The Wind Walker",
      price: "35€",
      catégorie : "Action-Aventure",
      id: 549765
    },

    {
      name: "Horizon : Forbidden West",
      price: "40€",
      catégorie : "Action-Aventure",
      id: 549766
    },

    {
      name: "Forza Horizon 5",
      price: "45€",
      catégorie : "Voiture",
      id: 549767
    },
    {
      name: "The Last Of Us",
      price: "55€",
      catégorie : "Survival horror",
      id: 549768
    },
    {
      name: "Red Dead Redemption II",
      price: "18€",
      catégorie : "Action-Aventure",
      id: 549769
    }
  ]);

  const [selectedId, setSelectedId] = useState();
  const [selectedId1, setSelectedId1] = useState();

    const data = [
      { id: '1', label: 'Indice 1' },
      { id: '2', label: 'Indice 2' },
      { id: '3', label: 'Indice 3' },
    ];

  const renderItem = ({ item }) => (
    <View style={styles.listItem}>
      <Text>{item.label}</Text>
    </View>
  );

  const [text, setText] = useState('');
  const [list, setList] = useState([]);

  const [price, setPrice] = useState('');
  const [categorie, setCategorie] = useState('');

  //tentative de filtre
  const [selectedCategory, setSelectedCategory] = useState('');

  const [filteredItems, setFilteredItems] = useState([]);

  const onPressAdd = (text,price,categorie) => {
    // setList([...list,text]);
    // console.log("liste ", list);

    const game = {
      name: text,
      price: price,
      catégorie: categorie,
      id: Date.now().toString(),
    };

    setList([...list, game]);
    setText(''); // Réinitialiser la valeur du TextInput
    setPrice(''); // Réinitialiser la valeur du TextInput
    setCategorie(''); // Réinitialiser la valeur du TextInput

  };

  function filterByCategory(category) {
    const filteredJeux = jeux.filter((jeu) => jeu.catégorie === category);
    const filteredList = list.filter((item) => item.catégorie === category);
  
    return (
      <>
        {filteredJeux.map((jeu) => (
          <Text style={styles.Text} key={jeu.id}>
            Nom: {jeu.name}, Prix: {jeu.price}, Catégorie: {jeu.catégorie}
            <Button
              title="trier (ji etais presque)"
              color="#841584"
              accessibilityLabel="Del"
              onPress={() => onPressDeleteJeu(jeu.id)}
            />
          </Text>
        ))}
  
        {filteredList.map((item) => (
          <Text style={styles.Text} key={item.id}>
            Nom: {item.name}, Prix: {item.price}, Catégorie: {item.catégorie}
            <Button
              title="x"
              color="#841584"
              accessibilityLabel="Del"
              onPress={() => onPressDelete(item.id)}
            />
          </Text>
        ))}
      </>
    );
  };

  const onPressCat = (selectedCategory) => {
    filterByCategory(selectedCategory);
  };

  const onPressDelete = (itemId) => {
    const updatedList = list.filter((item) => item.id !== itemId);
    setList(updatedList);
  };

  const onPressDeleteJeu = (itemId) => {
    const updatedJeux = jeux.filter((jeu) => jeu.id !== itemId);
    setJeux(updatedJeux);
  };

  return ( 

    <View style={styles.container}>
      <StatusBar style="auto" />


      <View style={styles.container2}>
          <Text style={styles.label}>Terminator</Text>
            <View style={{ flex: 1, flexDirection: 'row-reverse' }}>
              <Text style={styles.label}>Nombre de jeu vidéo : {jeux.length+list.length}</Text>
          </View>
      </View>

      <Text style={styles.Text}> Filter par catégorie </Text>
              <TextInput
                style={styles.input2}
                onChangeText={setSelectedCategory}
                value={selectedCategory}
                placeholder="Saisir catégorie">
              </TextInput>
              <Button
                  onPress={
                    () => onPressCat(selectedCategory)
                  }
                  title="trier (ji etais presque)"
                  color="#841584"
                  accessibilityLabel="Add">
              </Button>
      

      <ScrollView style={styles.scrol}>
          <View>

              {jeux.map((jeu) => (
              <Text style={styles.Text} key={jeu.id}>
                  Nom: {jeu.name}, Prix: {jeu.price}, Catégorie: {jeu.catégorie}
                  <Button
                    title="x"
                    color="#841584"
                    accessibilityLabel="Del"
                    onPress={() => onPressDeleteJeu(jeu.id)}>
                  </Button>
              </Text>
              ))}

              {list.map((list) => (
                  <Text style={styles.Text} key={list.id}>
                  Nom: {list.name}, Prix: {list.price}, Catégorie: {list.catégorie}
                  <Button
                    title="x"
                    color="#841584"
                    accessibilityLabel="Del"
                    onPress={() => onPressDelete(list.id)}>
                  </Button>
              </Text>
            ))}

          </View>
      </ScrollView>

      <View>

            <Text style={styles.Text}> Ajouter un jeu video : </Text>

              <Text style={styles.Text}> Titre : </Text>
              <TextInput
                style={styles.input2}
                onChangeText={setText}
                value={text}
                placeholder="Titre">
              </TextInput>

              <Text style={styles.Text}> Tarif : </Text>
              <TextInput
                style={styles.input2}
                onChangeText={setPrice}
                value={price}
                placeholder="Tarif">
              </TextInput>

              <Text style={styles.Text}> catégorie : </Text>
              <TextInput
                style={styles.input2}
                onChangeText={setCategorie}
                value={categorie}
                placeholder="Catégorie">
              </TextInput>

              <Button
                  onPress={
                    () => onPressAdd(text,price,categorie)
                  }
                  title="x"
                  color="#841584"
                  accessibilityLabel="Add">
              </Button>

              </View>
      {/* <Text>Open up App.js to start working on your app!</Text> */}
      {/* <StatusBar style="auto" /> */}

      
    </View>

    
  );
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: '15%',
  },
  image: {
    width: 200,
    height: 200,
  },
  Text: {
    color: 'blue',
    fontSize: 16,
    textAlign: 'center',
    marginTop: 10,
  },
  backgroundImage: {
    flex: 1,
    resizeMode: 'repeat',
    justifyContent: 'center',
    width: '100%',
    alignItems: 'center',  
  },
  scrol: {
    marginVertical: '10%',
    color: 'blue',
    textAlign: 'center',
    maxWidth: '85%',
    maxHeight: '40%',
  },
  container2: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginHorizontal: 20, // Ajoute de l'espace horizontal
  },
  label: {
    marginRight: 8,
  },
  input: {
    borderWidth: 1,
    padding: 8,
  },
  input2: {
    borderWidth: 1,
    padding: 8,
    backgroundColor: 'grey',
  },
});
